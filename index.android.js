import { AppRegistry } from 'react-native';

import Deals from './lib/Deals.android';

AppRegistry.registerComponent('Deals', () => Deals);
