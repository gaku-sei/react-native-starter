import { AppRegistry } from 'react-native';

import Deals from './lib/Deals.ios';

AppRegistry.registerComponent('Deals', () => Deals);
